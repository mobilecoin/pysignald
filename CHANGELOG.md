# Changelog


## Unreleased

### Fixes

* Add captcha parameter to register method. [Robert Aistleitner]

* Support attachments. [sh]

* Fix some API breakage. [Stavros Korokithakis]


